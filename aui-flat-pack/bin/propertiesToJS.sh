#!/bin/bash
#find script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd ../../ && pwd )"
echo $DIR
#Variables
LANGUAGE=$1
OUTPUT=$DIR/aui-flat-pack/target/filtered-resources/atlassian/auiplugin.properties.js
INPUT=$DIR/auiplugin/src/main/resources/auiplugin_$LANGUAGE.properties

# If no language is passed in or using en, just use the auiplugin.properties file
if [ "$LANGUAGE" = "en" ] || [ -z $LANGUAGE ]
then
INPUT=$DIR/auiplugin/src/main/resources/auiplugin.properties
fi

echo reading $INPUT and converting to $OUTPUT

#process properties file
awk -F= '
BEGIN {
    print "AJS.I18n.keys = {};"
}
{
    gsub(/ /,"",$1); 
    gsub(/^[ ]+/,"",$2);
    gsub(/\"/, "\\\"", $2);
    print "AJS.I18n.keys[\""$1"\"] = \"" $2 "\";";
}
END {
    
}
' $INPUT > $OUTPUT