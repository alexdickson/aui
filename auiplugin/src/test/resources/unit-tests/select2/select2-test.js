Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/external/jquery/plugins/jquery.select2.js');
Qunit.require('js/atlassian/aui-select2.js');

module("AUI Select2 Unit Tests", {
    setup: function(){
        this.$fixture =  $("#qunit-fixture");
        $("<select />").appendTo(this.$fixture);
    },
    teardown: function(){
    }
});

test("test for presence of AUI Select2", function() {
    equal(typeof $.fn.auiSelect2, "function", "AUI Select2 exists");
});

test("test for creation of AUI Select2", function() {
    var $select = $("select", this.$fixture);
    $select.auiSelect2();
    ok($select.prev(".aui-select2-container").length > 0, "AUI Select2 was created");
});