/**
 * Builds the AUI demo.json file that is uploaded as an artifact
 *
 * @param demoDirectory - First argument represents the directory of the compiled soy javascript
 * @param targetFile - Second argument represents the target FILE to output to
 * @param soyUtilsFile - Third argument is the absolute path to the location of the soyUtils.js file , this is required to run the soy javascript
 */

var fs = require("fs");

var demoDirectory = process.argv[2];
var targetFile = process.argv[3];
var soyUtilsFile = process.argv[4];

//Load soyUtils into memory
eval(fs.readFileSync(soyUtilsFile).toString());

//Load demos into memory
var demoFiles = fs.readdirSync(demoDirectory);
for (var i in demoFiles){
    //have to eval so that global objects created by these scripts are loaded properly.
    eval(fs.readFileSync(demoDirectory+"/"+demoFiles[i]).toString());
}

//construct the output using demos loaded above
var outputObject = [];
for(var j in demo) {
    var thisDemo = demo[j];
    outputObject.push({
        name: j,
        html: thisDemo.html() || "",
        javascript: thisDemo.javascript() || "",
        soy: thisDemo.soy ? thisDemo.soy() : ""
    });
}

//write output to specified file
fs.writeFileSync(targetFile, JSON.stringify(outputObject));





