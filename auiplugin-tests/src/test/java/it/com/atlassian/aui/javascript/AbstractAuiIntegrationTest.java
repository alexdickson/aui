package it.com.atlassian.aui.javascript;

import com.atlassian.pageobjects.aui.AuiTestedProduct;
import com.atlassian.webdriver.testing.rule.IgnoreBrowserRule;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.TestedProductRule;
import org.junit.Rule;

/**
 * @since 4.0
 */
public class AbstractAuiIntegrationTest
{
    @Rule public IgnoreBrowserRule ignoreBrowserRule = new IgnoreBrowserRule();
    @Rule public TestedProductRule<AuiTestedProduct> product =
            new TestedProductRule<AuiTestedProduct>(AuiTestedProduct.class);
    @Rule public SessionCleanupRule sessionCleanupRule = new SessionCleanupRule();


    protected boolean isWithinRange(int expected, int actual, int range)
    {
        return (actual >= expected - range && actual <= (expected + range));
    }
}
