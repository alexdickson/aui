package com.atlassian.pageobjects.aui.component.restfultable;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class RestfulTable
{

    private PageElement table;
    private EditRow createRow;
    private final String id;

    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder binder;

    @Inject
    private PageElementFinder finder;

    public RestfulTable(final String id)
    {
        this.id = id;
    }

    @Init
    private void getElements()
    {
        table = finder.find(By.id(id));
        createRow = binder.bind(EditRow.class,
                table.find(By.cssSelector(".aui-restfultable-create .aui-restfultable-row")));
    }

    public RestfulTable addEntry(final String... fields)
    {
        createRow.fill(fields).submit();
        return this;
    }

    public EditRow getCreateRow()
    {
        return createRow;
    }

    public Row getFirstRow()
    {
        return getRow(1);
    }

    public TimedCondition hasAnyRows()
    {
        return Conditions.and(
                table.timed().isPresent(),
                table.find(By.className("aui-restfultable-readonly")).timed().isVisible());
    }

    public Row getRow(int rowNumber)
    {
        return binder.bind(Row.class, table.find(getRowSelector(rowNumber)));
    }

    public Row waitForRow(int rowNumber)
    {
        PageElement element = table.find(getRowSelector(rowNumber));
        waitUntilTrue(element.timed().isPresent());
        return binder.bind(Row.class, element);
    }

    public boolean isEmpty()
    {
        return table.find(By.className("aui-restfultable-no-entires")).isPresent();
    }

    protected By getRowSelector(int rowNumber)
    {
        return By.cssSelector(String.format(".aui-restfultable-readonly:nth-of-type(%d)", rowNumber));
    }
}
